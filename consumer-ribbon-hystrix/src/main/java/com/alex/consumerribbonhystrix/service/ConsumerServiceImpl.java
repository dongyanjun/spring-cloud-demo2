package com.alex.consumerribbonhystrix.service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
public class ConsumerServiceImpl implements ConsumerService{
    @Autowired
    RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "fallback")//ignoreExceptions=Exception.class忽略异常降级
    //服务降级 默认如果抛出异常会自动降级
    //线程隔离 hystrix为每一个依赖服务都分配一个线程池
    //断路器 实现了对依赖资源故障的端口、对降级策略的自动切换以及对主逻辑的自动恢复机制
    //快照时间窗：断路器确定是否打开需要统计一些请求和错误数据，而统计的时间范围就是快照时间窗，默认为最近的10秒。
    //请求总数下限：在快照时间窗内，必须满足请求总数下限才有资格根据熔断。默认为20，意味着在10秒内，如果该hystrix命令的调用此时不足20次，即时所有的请求都超时或其他原因失败，断路器都不会打开。
    //错误百分比下限：当请求总数在快照时间窗内超过了下限，比如发生了30次调用，如果在这30次调用中，有16次发生了超时异常，也就是超过50%的错误百分比，在默认设定50%下限情况下，这时候就会将断路器打开。
    //断断路器打开后 降级逻辑会成为主逻辑 hystrix会启动一个休眠时间窗，在这个时间窗内，降级逻辑是临时的成为主逻辑
    //当休眠时间窗到期，断路器将进入半开状态，释放一次请求到原来的主逻辑上，如果此次请求正常返回，那么断路器将继续闭合，主逻辑恢复，如果这次请求依然有问题，断路器继续进入打开状态，休眠时间窗重新计时。
    public String consumer() {
        return restTemplate.getForObject("http://provider/dc", String.class);
    }

    public String fallback(Throwable throwable) {
        System.out.println(throwable.getMessage());
        return "fallback";
    }
}
