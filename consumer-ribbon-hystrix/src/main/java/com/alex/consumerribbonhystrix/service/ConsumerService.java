package com.alex.consumerribbonhystrix.service;

public interface ConsumerService {

    public String consumer();

    public String fallback(Throwable throwable);
}
