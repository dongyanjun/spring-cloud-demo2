package com.alex.consumerfeign.service;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;

@FeignClient("provider")
public interface DcClient {

    @GetMapping("/dc")
    String consumer();
}
